"""

Домашнее задание №1

Цикл for: Оценки

* Создать список из словарей с оценками учеников разных классов 
  школы вида [{'school_class': '4a', 'scores': [3,4,4,5,2]}, ...]
* Посчитать и вывести средний балл по всей школе.
* Посчитать и вывести средний балл по каждому классу.
"""

def main():
    """
    Эта функция вызывается автоматически при запуске скрипта в консоли
    В ней надо заменить pass на ваш код
    """
    scores_dic = [
        {'school_class': '1a', 'scores': [3,4,4,5,2]},
        {'school_class': '2a', 'scores': [5,5,5,5,2]},
        {'school_class': '3a', 'scores': [3,1,1,5,2]},
        {'school_class': '4a', 'scores': [3,2,4,5,1]},
    ]
    sum = 0
    number_of_scores = 0
    for dic in scores_dic:
        for score in dic["scores"]:
            sum = sum + int(score)
            number_of_scores += 1
    mean_score = sum / number_of_scores
    print(mean_score)
    
if __name__ == "__main__":
    main()
